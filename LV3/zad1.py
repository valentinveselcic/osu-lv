# Zadatak 3.4.1 Skripta zadatak_1.py uˇcitava podatkovni skup iz data_C02_emission.csv.
# Dodajte programski kod u skriptu pomo ́cu kojeg možete odgovoriti na sljede ́ca pitanja:




import pandas as pd
import numpy as np


D = pd.read_csv('LV3\data_C02_emission.csv')

# a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka veliˇcina? Postoje li izostale ili
# duplicirane vrijednosti? Obrišite ih ako postoje. Kategoriˇcke veliˇcine konvertirajte u tip
# category.
print(D.info)
# 2211 mjerenja
print(type(D['Fuel Consumption Hwy (L/100km)']))
#Data types: Series
print(D.isnull().sum())
D.dropna
print(D.isnull().sum())
D.drop_duplicates

D[D.columns].astype('category')
print(D.dtypes)

# b) Koja tri automobila ima najve ́cu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
# ime proizvo  ̄daˇca, model vozila i kolika je gradska potrošnja.

print(f"Tri automobila sa najvecom potrosnjom: {D[['Make', 'Model', 'Fuel Consumption City (L/100km)']].nlargest(3, ['Fuel Consumption City (L/100km)'])}; Tri sa najmanjom {D[['Make', 'Model', 'Fuel Consumption City (L/100km)']].nsmallest(3, ['Fuel Consumption City (L/100km)'])}")

# c) Koliko vozila ima veliˇcinu motora izme  ̄du 2.5 i 3.5 L? Kolika je prosjeˇcna C02 emisija
# plinova za ova vozila?

motor_between_values = (D['Engine Size (L)'] > 2.5) & (D['Engine Size (L)'] < 3.5)
print(f"Broj vozila sa motorima izmedju 2.5 i 3.5L: {motor_between_values.sum()}")
print(f"Njihove prosjecne C02 emisije: {D['CO2 Emissions (g/km)'][motor_between_values].mean()}")

# d) Koliko mjerenja se odnosi na vozila proizvo  ̄daˇca Audi? Kolika je prosjeˇcna emisija C02
# plinova automobila proizvo  ̄daˇca Audi koji imaju 4 cilindara?
audi_vehicles = (D['Make'] == 'Audi')
print(f"Broj audi vozila sa 4 cilindra: {audi_vehicles.sum()}")
audi_with_4cylinders = (D['Make'] == 'Audi') & (D['Cylinders'] == 4)
print(f"Njihove prosjecne CO2 emisije: {D['CO2 Emissions (g/km)'][audi_with_4cylinders].mean()}")

# e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjeˇcna emisija C02 plinova s obzirom na
# broj cilindara?
vehicles_with_cylinders = (D['Cylinders'] % 2 == 0)
groupedby_cylinders = D[vehicles_with_cylinders].groupby(['Cylinders'])
print(f"Broj vozila sa 4,6,8... cilindara je: {groupedby_cylinders.size()}")
print(f"Prosjecne emisije s ozbirom na broj cilindara su: {groupedby_cylinders['CO2 Emissions (g/km)'].mean()}")
# f) Kolika je prosjeˇcna gradska potrošnja u sluˇcaju vozila koja koriste dizel, a kolika za vozila
# koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
gasoline_vehicles = (D['Fuel Type'] == 'X') | (D['Fuel Type'] == 'Z')
diesel_vehicles = (D['Fuel Type'] == 'D')
print(f"Prksjecna gradska potrosnja za benzin: {D['Fuel Consumption City (L/100km)'][gasoline_vehicles].mean()}, median {D['Fuel Consumption City (L/100km)'][gasoline_vehicles].median()}")
print(f"Prksjecna gradska potrosnja za dizel: {D['Fuel Consumption City (L/100km)'][diesel_vehicles].mean()}, median {D['Fuel Consumption City (L/100km)'][diesel_vehicles].mean()}")
# g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najve ́cu gradsku potrošnju goriva?
print(f"Vozilo sa 4 cilindra sa najvecom potrosnjom: \n{D.loc[D['Fuel Consumption City (L/100km)'][(D['Cylinders'] == 4) & (D['Fuel Type'] == 'D')].idxmax()]}")
# h) Koliko ima vozila ima ruˇcni tip mjenjaˇca (bez obzira na broj brzina)?
manual_vehicles = (D['Transmission'].str.contains('^M'))
print(f"Manualnih vozila: {manual_vehicles.sum()}")
# i) Izraˇcunajte korelaciju izme  ̄du numeriˇckih veliˇcina. Komentirajte dobiveni rezultat.
print(D.corr(numeric_only = True))

#1 odlicna korelacija, ako se jedna velicina poveca i ostale ce vjerovatno, 0.9 vrlo dobra korelacija, oko 0.2 je losa korelacija, negativne korelacije: ako se jedna velicina poveca druge ce se smanjiti