# Zadatak 3.4.2 Napišite programski kod koji  ́ce prikazati sljede ́ce vizualizacije:
# a) Pomo ́cu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.
# b) Pomo ́cu dijagrama raspršenja prikažite odnos izme  ̄du gradske potrošnje goriva i emisije
# C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izme  ̄du
# veliˇcina, obojite toˇckice na dijagramu raspršenja s obzirom na tip goriva.
# c) Pomo ́cu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip
# goriva. Primje ́cujete li grubu mjernu pogrešku u podacima?
# d) Pomo ́cu stupˇcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu
# groupby.
# e) Pomo ́cu stupˇcastog grafa prikažite na istoj slici prosjeˇcnu C02 emisiju vozila s obzirom na
# broj cilindara.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

D = pd.read_csv("LV3\data_C02_emission.csv")
D.drop_duplicates
D.dropna
#a
#Najvise je vozila sa potrosnjom izmedju 200 i 300, mali broj outliera iznad 500
plt.hist(D['CO2 Emissions (g/km)'])
plt.show()