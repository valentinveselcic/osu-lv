import matplotlib.pyplot as plt
import numpy as np

data = np.genfromtxt("data.csv", delimiter=",")
data = np.delete(data, (0), axis=0)

#a)
print(f"a) {len(data)}")

#b)

# height = data[:, 1]
# weight = data[:, 2]
# plt.scatter(height, weight)
# plt.show()

#c)

height = data[::50, 1]
weight = data[::50, 2]
plt.scatter(height, weight)
# plt.show()

#d)

print(height.min())
print(height.max())
print(height.mean())

#e)

male_heights = (data[:, 0] == 1)
female_heights = (data[:, 0] == 0)

print("Male heights:")
print(data[male_heights, 1].min())
print(data[male_heights, 1].max())
print(data[male_heights, 1].mean())
print("Female heights:")
print(data[female_heights, 1].min())
print(data[female_heights, 1].max())
print(data[female_heights, 1].mean())