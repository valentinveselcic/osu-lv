import numpy as np
import matplotlib.pyplot as plt

#a)
img = plt.imread("road.jpg")

img = img[:,:,0].copy()
print(img)
brightness = 200 #0-255
for x in range(len(img)):
    for y in range(len(img[x])):
        if(img[x, y] < 255 - brightness):
            img[x, y] += brightness
        else:
            img[x, y] = 255

print(img)
plt.imshow(img, cmap= "gray")
plt.show()

#b)

first_limit = int(len(img[0]) * 0.25)
second_limit = int(len(img[0]) * 0.5)

img = img[:,first_limit:second_limit]
plt.imshow(img, cmap="gray")
plt.show()

#c)
plt.imshow(np.rot90(np.rot90(np.rot90(img))), cmap = "gray")
plt.show()

#d)

img[:,:] = img[:,::-1]

plt.imshow(img, cmap = "gray")
plt.show()