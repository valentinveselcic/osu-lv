import numpy as np
import matplotlib.pyplot as plt

ones = np.ones((25, 25))
zeros = np.zeros((25, 25))

first_col = np.vstack((ones, zeros))
second_col = np.vstack((zeros, ones))

pic = np.hstack((second_col, first_col))

plt.imshow(pic, cmap= "gray")
plt.show()
