# Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
# sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
# potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
# vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
# (npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku.

def main():
    nums = []
    while True:
        num = input("Input a number or 'Done' ")
        if num == "Done":
            break
        try:
            nums.append(int(num))
        except:
            print("Not a number!")
    print(nums.sort)     
    print(f"Uneseno {len(nums)} brojeva")
    print("Average: " + str(sum(nums)/ len(nums)))
    print("Max: "+ str(max(nums)))
    print("Min: "+str(min(nums)))


if __name__ == "__main__":
    main()