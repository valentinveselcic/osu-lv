# a) Izracunajte koliki je prosje ˇ can broj rije ˇ ci u SMS porukama koje su tipa ham, a koliko je ˇ
# prosjecan broj rije ˇ ci u porukama koje su tipa spam. ˇ
# b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?

def main():
    ham_count = 0
    ham_messages = 0
    spam_count = 0
    spam_messages = 0
    spam_ends_with_exclamation = 0
    with open('SMSSpamCollection.txt') as f:
        line = f.readline().strip().split()
        while line:
            if line[0] == "ham":
                ham_count += 1
                ham_messages += len(line) - 1
            elif line[0] == "spam":
                spam_count += 1
                spam_messages += len(line) - 1
                if line[-1].endswith('!'):
                    spam_ends_with_exclamation += 1
            line = f.readline().strip().split()
    print(f"Ham count: {ham_count}, ham messages: {ham_messages}, average {ham_messages/ham_count}")
    print(f"Spam count: {spam_count}, spam messages: {spam_messages}, average {spam_messages/spam_count}")
    print(f"Spam messages ending with '!': {spam_ends_with_exclamation}")
            

if __name__ == "__main__":
    main()