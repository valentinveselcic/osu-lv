# Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
# Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci koje se pojavljuju u ˇ
# datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) pojavljuje u datoteci. ˇ
# Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

def main():
    with open("song.txt") as f:
        D = f.read().strip()
    words = D.split()
    song_dict = {}
    for word in words:
        if word not in song_dict.keys():
            song_dict[word] = 1
        else:
            song_dict[word] += 1
    
    one_time_words = 0
    for key in song_dict.keys():
        if song_dict[key] == 1:
            print(key)
            one_time_words += 1
    print(one_time_words)

if __name__ == "__main__":
    main()