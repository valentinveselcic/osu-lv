# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F


def main():
    
    try:
        points = float(input("Unesite broj bodova (0.0 - 1.0) "))
        if points < 0.0 or points > 1.0:
            print("Number out of bounds!")
        else:
            print(points_calculator(points))
    except:
        print("Not a number inputted!")
        
def points_calculator(points):
    if points >= 0.9:
        return "A"
    if points >= 0.8:
        return "B"
    if points >= 0.7:
        return "C"
    if points >= 0.6:
        return "D"
    if points < 0.6:
        return "F"




if __name__ == "__main__":
    main()