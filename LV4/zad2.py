import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import root_mean_squared_error
from sklearn.metrics import r2_score
from sklearn.preprocessing import OneHotEncoder

x = ['Fuel Type','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']
y = ['CO2 Emissions (g/km)']

data = pd.read_csv('data_C02_emission.csv')
x = data[x].to_numpy()
y = data[y].to_numpy()

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state= 1)

ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()
x_test = np.concatenate((x_test, X_encoded))
x_train = np.concatenate((x_train, X_encoded))

linearModel = lm.LinearRegression()
linearModel.fit(x_train, y_train)

y_test_p = linearModel.predict(x_test)


error = abs(y_test_p - y_test)
max_error_id = np.argmax(error)
print(data.loc[max_error_id][0])