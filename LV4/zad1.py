import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import root_mean_squared_error
from sklearn.metrics import r2_score
from sklearn.preprocessing import OneHotEncoder

x = ['Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']
y = ['CO2 Emissions (g/km)']

data = pd.read_csv('data_C02_emission.csv')
x = data[x].to_numpy()
y = data[y].to_numpy()

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state= 1)

plt.scatter(x_train[:, 0], y_train, label = "Training data")
plt.scatter(x_test[:, 0], y_test, label = "Test data")
plt.legend()
plt.show()


sc = MinMaxScaler()

x_train_n = sc.fit_transform(x_train)
x_test_n = sc.fit_transform(x_test)

plt.hist(x_train)
plt.show()
plt.hist(x_train_n)
plt.show()

linearModel = lm.LinearRegression()
linearModel.fit(x_train_n, y_train)

y_test_p = linearModel.predict(x_test_n)
print(y_test_p)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
RMSE = root_mean_squared_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print(f"MAE: {MAE}")
print(f"MAPE: {MAPE}")
print(f"RMSE: {RMSE}")
print(f"R2: {R2}")


plt.scatter(x_test_n[:, 0], y_test, label = "Real values")
plt.scatter(x_test_n[:, 0], y_test_p, label = "Predicted values")
plt.legend()
plt.show()


#20% test data
# MAE: 9.064494397330357
# MAPE: 0.03372907304652182
# RMSE: 18.304506422984684
# R2: 0.9171622966329126

#30% test data
# MAE: 8.513749196433858
# MAPE: 0.031214080030826995
# RMSE: 17.286809050572735
# R2: 0.9209738863191319

#sa povecanjem broja vrijednosti ulaznog skupa sposobnost predikcije se poboljsava, sto rezultira u povoljnijim vrijednostima metrika

ohe = OneHotEncoder()
X_encoded = ohe.fit_transform(data[['Fuel Type']]).toarray()