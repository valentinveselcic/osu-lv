import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# 2. Pomo ́cu matplotlib biblioteke prikažite jednu sliku iz skupa podataka za uˇcenje te ispišite
# njezinu oznaku u terminal.

# TODO: prikazi nekoliko slika iz train skupa
# plt.imshow(x_train[0])
print(y_train[0])
# plt.show()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")

y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

# 1. Upoznajte se s uˇcitanim podacima. Koliko primjera sadrži skup za uˇcenje, a koliko skup za
# testiranje? Kako su skalirani ulazni podaci tj. slike? Kako je kodirana izlazne veliˇcina?
#Kodirane su u vektoru od 10 vrijednosti, 60k primjera u skupu za ucenje, 10k za testiranje


x_train_2 = x_train.reshape(60000, 784)
x_test_2 = x_test.reshape(10000, 784)
# 3. Pomo ́cu klase Sequential izgradite mrežu prikazanu na slici 8.5. Pomo ́cu metode
# .summary ispišite informacije o mreži u terminal.

model = keras.Sequential()

model.add(layers.Dense(100, activation ="relu", input_shape = (784, )))
model.add(layers.Dense(50, activation ="relu"))
model.add(layers.Dense(10, activation ="softmax"))
model.summary ()

# 4. Pomo ́cu metode .compile podesite proces treniranja mreže.
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy",])
# 5. Pokrenite uˇcenje mreže (samostalno definirajte broj epoha i veliˇcinu serije). Pratite tijek
# uˇcenja u terminalu.
batch_size = 32
epochs = 2
history = model.fit(x_train_2 ,
y_train_s,
batch_size = batch_size ,
epochs = epochs ,
validation_split = 0.1)
# 6. Izvršite evaluaciju mreže na testnom skupu podataka pomo ́cu metode .evaluate.
score = model.evaluate(x_test_2 , y_test_s , verbose =0)
# 7. Izraˇcunajte predikciju mreže za skup podataka za testiranje. Pomo ́cu scikit-learn biblioteke
# prikažite matricu zabune za skup podataka za testiranje.
predictions = model.predict(x_test_2)
y_test_s = np.argmax(y_test_s, axis = 1)
predictions = np.argmax(predictions, axis = 1)
ConfusionMatrixDisplay(confusion_matrix = confusion_matrix(y_test_s, predictions)).plot()
plt.show()
# 8. Pohranite model na tvrdi disk.
model.save("FCN/")
# pretvori labele
  


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu



# TODO: definiraj karakteristike procesa ucenja pomocu .compile()



# TODO: provedi ucenje mreze



# TODO: Prikazi test accuracy i matricu zabune



# TODO: spremi model

