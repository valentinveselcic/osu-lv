# 2 Napišite skriptu koja  ́ce uˇcitati izgra  ̄denu mrežu iz zadatka 1 i MNIST skup
# podataka. Pomo ́cu matplotlib biblioteke potrebno je prikazati nekoliko loše klasificiranih slika iz
# skupa podataka za testiranje. Pri tome u naslov slike napišite stvarnu oznaku i oznaku predvi  ̄denu
# mrežom.
from keras.models import load_model
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt

model = keras.Sequential()

model = load_model('FCN/')
model.summary()

# 3 Napišite skriptu koja  ́ce uˇcitati izgra  ̄denu mrežu iz zadatka 1. Nadalje, skripta
# treba uˇcitati sliku test.png sa diska. Dodajte u skriptu kod koji  ́ce prilagoditi sliku za mrežu,
# klasificirati sliku pomo ́cu izgra  ̄dene mreže te ispisati rezultat u terminal. Promijenite sliku
# pomo ́cu nekog grafiˇckog alata (npr. pomo ́cu Windows Paint-a nacrtajte broj 2) i ponovo pokrenite
# skriptu. Komentirajte dobivene rezultate za razliˇcite napisane znamenke.

im = plt.imread("Untitled.png").convert('L')
im = im.astype("float32") / 255
print(im)
prediction = model.predict(im)
print(prediction)