import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans
from PIL import Image as im

# ucitaj sliku
# img = Image.imread("imgs\\imgs\\test_1.jpg")
# img = Image.imread("imgs\\imgs\\test_2.jpg")
# img = Image.imread("imgs\\imgs\\test_3.jpg")
# img = Image.imread("imgs\\imgs\\test_4.jpg")
# img = Image.imread("imgs\\imgs\\test_5.jpg")
img = Image.imread("imgs\\imgs\\test_6.jpg")


# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

unique = np.unique(img_array, axis=0)
colors = unique.shape[0]
print("Broj boja: ", colors)

k = 3
km = KMeans(n_clusters = k, init='random', n_init=5, random_state =0)
km.fit(img_array_aprox)
labels = km.predict(img_array_aprox)
centers = km.cluster_centers_

for i in range(0, k):
    img_array_aprox[ i == labels] = centers[i] 

img_array_new = np.reshape(img_array_aprox, (w,h,d))
plt.figure()
plt.title("Rezultantna slika")
plt.imshow(img_array_new)
plt.tight_layout()
plt.show()

#Povecanjem broja grupa povecava se broj boja na slici


sse = []
for k in range(1, 11):
    kmeans = KMeans(n_clusters =k, init='random', n_init=5, random_state =0)
    kmeans.fit(img_array_aprox)
    sse.append(kmeans.inertia_)

plt.plot(range(1, 11), sse)
plt.xticks(range(1, 11))
plt.xlabel("Number of Clusters")
plt.ylabel("SSE")
plt.show()

for i in range(km.n_clusters):
    binary_img = np.zeros((w,h,d))
    imgK_reshaped = labels.reshape((w, h))
    binary_img[imgK_reshaped == i] = 1
    plt.figure()
    plt.title("Grupa " + str(i+1))
    plt.imshow(binary_img)
    plt.show()

#Neke od RGB vrijednosti su zastupljenije od ostalih na slikama