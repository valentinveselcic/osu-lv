import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn . metrics import accuracy_score
from sklearn . metrics import confusion_matrix , ConfusionMatrixDisplay

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


# a) Prikažite podatke za ucenje u ˇ x1 −x2 ravnini matplotlib biblioteke pri cemu podatke obojite ˇ
# s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
# marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
# cmap kojima je moguce de ´ finirati boju svake klase.
plt.scatter(y = np.transpose(X_train)[1], x = np.transpose(X_train)[0], c = y_train, label = 'Training')
plt.scatter(y = np.transpose(X_test)[1], x = np.transpose(X_test)[0], marker= 'X', c = y_test, label = 'Test')
plt.legend()
plt.show()

# b) Izgradite model logisticke regresije pomo ˇ cu scikit-learn biblioteke na temelju skupa poda- ´
# taka za ucenje.
lrm = LogisticRegression()
lrm.fit(X_train, y_train)

# c) Pronadite u atributima izgra ¯ denog modela parametre modela. Prikažite granicu odluke ¯
# naucenog modela u ravnini ˇ x1 − x2 zajedno s podacima za ucenje. Napomena: granica ˇ
# odluke u ravnini x1 −x2 definirana je kao krivulja: θ0 +θ1x1 +θ2x2 = 0.
inter = lrm.intercept_[0]
t1,t2 = lrm.coef_.T
plt.figure()
plt.scatter(y = np.transpose(X_train)[1], x = np.transpose(X_train)[0], c = y_train, label = 'Training')
od1 = -((inter / t2) + np.transpose(X_train)[0] * (t1/t2))
plt.plot(np.transpose(X_train)[0], od1)
# d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgra ´ denog modela logisti ¯ cke ˇ
# regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izra ˇ cunate to ˇ cnost, ˇ
# preciznost i odziv na skupu podataka za testiranje

y_test_p = lrm.predict(X_test)
cm = confusion_matrix( y_test , y_test_p )
print ("Matrica zabune : " , cm )
disp = ConfusionMatrixDisplay ( confusion_matrix ( y_test , y_test_p ) )
disp . plot ()
plt . show ()