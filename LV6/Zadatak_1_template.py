import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

from sklearn.model_selection import cross_val_score


def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                           np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


# ucitaj podatke
data = pd.read_csv("Social_Network_Ads.csv")
print(data.info())

data.hist()
plt.show()

# dataframe u numpy
X = data[["Age", "EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, stratify=y, random_state=10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

# Model logisticke regresije
LogReg_model = LogisticRegression(penalty=None)
LogReg_model.fit(X_train_n, y_train)

# Evaluacija modela logisticke regresije
y_train_p = LogReg_model.predict(X_train_n)
y_test_p = LogReg_model.predict(X_test_n)

print("Logisticka regresija: ")
print("Tocnost train: " +
      "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p))))

# granica odluke pomocu logisticke regresije
plot_decision_regions(X_train_n, y_train, classifier=LogReg_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
plt.show()


# 1. Izradite algoritam KNN na skupu podataka za ucenje (uz ˇ K=5). Izracunajte to ˇ cnost ˇ
# klasifikacije na skupu podataka za ucenje i skupu podataka za testiranje. Usporedite ˇ
# dobivene rezultate s rezultatima logisticke regresije. Što primje ˇ cujete vezano uz dobivenu ´
# granicu odluke KNN modela?

KNN_model = KNeighborsClassifier(n_neighbors=5)
KNN_model.fit(X_train_n, y_train)

y_train_p = KNN_model.predict(X_train_n)
y_test_p = KNN_model.predict(X_test_n)

print("KNN: ")
print(f"Tocnost train: {accuracy_score(y_train, y_train_p)}")
print(f"Tocnost test: {accuracy_score(y_test, y_test_p)}")

plot_decision_regions(X_train_n, y_train, classifier=KNN_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("KNN Tocnost: " +
          "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
plt.show()

# Za dani problem KNN se pokazuje kao primjereniji algoritam od Logisticke regresije, sa povoljnijom vrijednoscu tocnosti klasifikacije

# KNN n_neighbors = 5:
# Tocnost train: 0.921875
# Tocnost test: 0.9125

# KNN n_neighbors = 1:
# Tocnost train: 0.99375
# Tocnost test: 0.85

# KNN n_neighbors = 100:
# Tocnost train: 0.796875
# Tocnost test: 0.825

# Vidljivo je kako i kada uzimamo i 1 i 100 susjeda model ne prikazuje najbolje problem, u prvom slucaju tocnost treninga je velika dok je testa u usporebi znatno manja

# 2. Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra ´ K
# algoritma KNN za podatke iz Zadatka 1.

scores = cross_val_score(KNN_model, X_train, y_train, cv=5)
print(scores)

param_grid = {'n_neighbors': [1, 5, 10, 20, 30, 50, 80, 100]}
svm_gscv = GridSearchCV(KNN_model, param_grid, cv=5, scoring='accuracy')
svm_gscv.fit(X_train_n, y_train)
print(svm_gscv.best_params_)
print(svm_gscv.best_score_)

# 3. Na podatke iz Zadatka 1 primijenite SVM model koji koristi RBF kernel funkciju
# te prikažite dobivenu granicu odluke. Mijenjajte vrijednost hiperparametra C i γ. Kako promjena
# ovih hiperparametara utjece na granicu odluke te pogrešku na skupu podataka za testiranje? ˇ
# Mijenjajte tip kernela koji se koristi. Što primjecujete?

SVM_model = svm.SVC(kernel='rbf', C=0.5, gamma=2)
SVM_model.fit(X_train_n, y_train)
y_test_p = SVM_model.predict(X_test_n)
y_train_p = SVM_model.predict(X_train_n)

plot_decision_regions(X_train_n, y_train, classifier=SVM_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost SVM: " +
          "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
plt.show()

# Promjena parametra C odredjuje tezinsku vrijednost koja ce se dodijeliti vrijednostima izvan klasifikacije sa povecanjem vrijednosti
# dolazi do promjene oblika granice klasifikacija te vrijednosti koje one obuhvacaju, gamma je samo koeficijent funkcije kernela

# Promjenom funkcije kernela dolazi do promjene u obliku granica izmedju klasifikacija
param_grid = {'C': [0.2, 0.3, 0.5, 1, 1.5, 2, 2.50],
              'gamma': [1, 2, 3, 5, 8, 13]}
svm_gscv = GridSearchCV(SVM_model, param_grid, cv=5, scoring='accuracy')
svm_gscv.fit(X_train_n, y_train)
print(svm_gscv.best_params_)
print(svm_gscv.best_score_)
